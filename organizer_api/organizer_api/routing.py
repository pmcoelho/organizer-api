from rest_framework import routers

from tags.api.urls import router as tags_router
from til.api.urls import router as til_router


def setup_router():
    router = routers.DefaultRouter()

    router.registry.extend(tags_router.registry)
    router.registry.extend(til_router.registry)

    return router
