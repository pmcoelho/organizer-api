from django.contrib import admin

from til.models import TilJournal, TilEntry


class TilEntryInline(admin.TabularInline):
    model = TilEntry
    extra = 1


class TilJournalAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "owner", "created_at", "updated_at")
    inlines = (TilEntryInline, )


class TilEntryAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "owner", "created_at", "updated_at")


admin.site.register(TilJournal, TilJournalAdmin)
admin.site.register(TilEntry, TilEntryAdmin)
