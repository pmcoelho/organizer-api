from rest_framework import viewsets
from rest_framework_extensions.mixins import NestedViewSetMixin
from django.shortcuts import get_object_or_404

from ..models import TilJournal, TilEntry
from .serializers import TilJournalSerializer, TilEntrySerializer


class TilJournalViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = TilJournal.objects.all()
    serializer_class = TilJournalSerializer

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user)

    def perform_create(self, serializer):
        return serializer.save(owner=self.request.user)


class TilEntryViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = TilEntry.objects.all()
    serializer_class = TilEntrySerializer

    def get_queryset(self):
        journal_id = self.kwargs.get('parent_lookup_journal_id')
        journal = get_object_or_404(TilJournal, pk=journal_id, owner=self.request.user)
        return self.queryset.filter(owner=self.request.user, journal=journal)

    def perform_create(self, serializer):
        journal_id = self.kwargs.get('parent_lookup_journal_id')
        journal = get_object_or_404(TilJournal, pk=journal_id, owner=self.request.user)
        return serializer.save(owner=self.request.user, journal=journal)
