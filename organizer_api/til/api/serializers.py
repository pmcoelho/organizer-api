from rest_framework import serializers

from ..models import TilJournal, TilEntry


class TilJournalSerializer(serializers.ModelSerializer):
    class Meta:
        model = TilJournal
        fields = (
            "id",
            "title",
            "description",
            "tags",
            "owner",
            "created_at",
            "updated_at",
        )
        read_only_fields = ("id", "owner", "created_at", "updated_at")


class TilEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = TilEntry
        fields = (
            "id",
            "title",
            "description",
            "journal",
            "tags",
            "owner",
            "created_at",
            "updated_at",
        )
        read_only_fields = ("id", "owner", "journal", "created_at", "updated_at")
