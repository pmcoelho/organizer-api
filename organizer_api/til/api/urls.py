from rest_framework_extensions.routers import ExtendedSimpleRouter

from .views import TilJournalViewSet, TilEntryViewSet


router = ExtendedSimpleRouter()

router.register(r"til-journals", TilJournalViewSet, basename="til-journal").register(
    r"entries",
    TilEntryViewSet,
    basename="til-journal-entries",
    parents_query_lookups=["journal_id"],
)
