from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from tags.models import Tag


class TilJournal(models.Model):
    title = models.CharField(_("title"), max_length=255)
    description = models.TextField(_("description"), null=True, blank=True)

    tags = models.ManyToManyField(Tag, blank=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(_("created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("updated_at"), auto_now=True)

    class Meta:
        verbose_name = _("TIL Journal")
        verbose_name_plural = _("TIL Journals")
        default_related_name = "til_journals"

    def __str__(self):
        return self.title


class TilEntry(models.Model):
    title = models.CharField(_("title"), max_length=255)
    description = models.TextField(_("description"), null=True, blank=True)

    tags = models.ManyToManyField(Tag, blank=True)
    journal = models.ForeignKey(
        TilJournal, related_name="entries", on_delete=models.CASCADE
    )
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(_("created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("updated_at"), auto_now=True)

    class Meta:
        verbose_name = _("TIL Journal")
        verbose_name_plural = _("TIL Journals")
        default_related_name = "til_entries"

    def __str__(self):
        return self.title
