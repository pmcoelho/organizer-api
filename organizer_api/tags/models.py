from django.db import models
from django.utils.translation import ugettext_lazy as _
from colorfield.fields import ColorField


class Tag(models.Model):
    name = models.CharField(_("name"), max_length=255)
    color = ColorField()

    created_at = models.DateTimeField(_("created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("updated_at"), auto_now=True)

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")
        default_related_name = "tags"
