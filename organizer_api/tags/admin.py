from django.contrib import admin

from til.models import TilJournal, TilEntry
from .models import Tag


class TilJournalInline(admin.TabularInline):
    model = TilJournal.tags.through
    extra = 1


class TilEntryInline(admin.TabularInline):
    model = TilEntry.tags.through
    extra = 1


class TagAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "color")
    inlines = (TilJournalInline, TilEntryInline)


admin.site.register(Tag, TagAdmin)
