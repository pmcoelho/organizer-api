from rest_framework import serializers

from ..models import Tag


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'name', 'color', 'created_at', 'updated_at')
        read_only_fields = ('id', 'created_at', 'updated_at')
